 /*Calculate Ring Size*/
 $(document).ready(function(){
 /*Change mm/inch*/
	 $('body').on('click','.units_wrapper button',function (e) {
	 	$(this).parent().children().removeClass('active');
	 	$(this).addClass('active');
	 	$(this).parents('.result_block').find('.result_us').removeClass('error').html('')
	 })
	 /*Click on calculate button*/
	 let clearRingSizeOutput = (e) => {
	 		$(e.currentTarget).parents('.result_block').find('.result_form ').addClass('error');
     	$(e.currentTarget).parents('.result_block').find('.result_us').html('');
	 }
	 let fillRingSizeOutput = (sizeHtml, e) => {
      sizeHtml = sizeHtml + ' US'
      $(e.currentTarget).parents('.result_block').find('.result_form ').removeClass('error');
      $(e.currentTarget).parents('.result_block').find('.result_us').html(sizeHtml);
	 }

    $('body').on('click','[data-calculate-size]',function (e) { 
      launchRingSizeCalculate(e);
    })
    $(".result_form input").keyup(function(e){
    if(e.keyCode == 13){
        e.preventDefault();
        launchRingSizeCalculate(e);
      }
    });
    function launchRingSizeCalculate(e) {
      let input_value = parseFloat($(e.currentTarget).parent().children('input').val());
      let type = $(e.currentTarget).parents('.result_block').find('.measure.active').attr('data-type');
      let units = $(e.currentTarget).parents('.result_block').find('.measure.active').attr('data-units');
       if(typeof input_value == 'number' && isNaN(input_value) == false){
           $(e.currentTarget).parent().removeClass('error');
           let sizeHtml = inputRingSize(type, units, input_value);
           if (sizeHtml == false) {
            clearRingSizeOutput(e);
           } else {
            fillRingSizeOutput(sizeHtml, e);
           }
       } else if (isNaN(input_value)){
           clearRingSizeOutput(e);
       }
    }
    function returnHtmlSizeUSA(sizeUSA) {
      /* returnHtmlSizeUSA('4-1_2') - return 4 1/2 */
      let sizeSplit = sizeUSA.split("-")
      let sizeLength = sizeSplit.length;

      if (sizeLength == 1) {
          let whole = sizeSplit[0];
          whole = '<span class="whole">'+whole+'</span>'
          return whole
      } else if (sizeLength >= 2) {
          let whole = sizeSplit[0];
              whole = '<span class="whole">'+whole+'</span>'

          let fraction = sizeSplit[1];
          fraction = fraction.split("_");

          if (fraction.length >= 2) {
              fraction = '<span class="fraction"><span class="top">'+fraction[0]+'</span><span class="bottom">'+fraction[1]+'</span></span>'
              let size = whole + fraction;

              return size                    
          }   else {                    
              return whole 
          }
      }
    }
      function inputRingSize(sizeType, measure, value) {
        let calculateRingSize = (sizeСhart, minVal, maxVal, value) => {
          for (i in sizeСhart) {
          	i = parseFloat(i);       
          	if (value >= minVal && value <= maxVal) {
	            if (i >= value) {		          
	              return returnHtmlSizeUSA(sizeСhart[i]);
	            }
          	}  
          	else {
            	/*Shop invalid data error*/            	
            	return false
            }
          }          	
        }
        let method = sizeType + '-' + measure;
        switch (method) {
          case 'finger-mm':            
            var sizeСhart = {
              47.9: '4-1_2',
              49.3: '5',
              50.6: '5-1_2',
              51.8: '6',
              53.1: '6-1_2',
              54.5: '7',
              55.7: '7-1_2',
              57.1: '8',
              58.2: '8-1_2',
              59.3: '9',
              60.9: '9-1_2',
              62.3: '10',
              63.4: '10-1_2'	
            }
            return calculateRingSize(sizeСhart, 47.9, 63.4, value);
            break;
          case 'finger-inch':
            var sizeСhart = {
              1.89: '4-1_2',
              1.94: '5',
              1.99: '5-1_2',
              2.04: '6',
              2.09: '6-1_2',
              2.14: '7',
              2.19: '7-1_2',
              2.25: '8',
              2.29: '8-1_2',
              2.34: '9',
              2.40: '9-1_2',
              2.45: '10',
              2.50: '10-1_2'	
            }
            return calculateRingSize(sizeСhart, 1.89, 2.50, value);
            break;
          case 'ring-mm':
            var sizeСhart = {
              15.3: '4-1_2',
              15.7: '5',
              16.1: '5-1_2',
              16.5: '6',
              16.9: '6-1_2',
              17.4: '7',
              17.8: '7-1_2',
              18.2: '8',
              18.5: '8-1_2',
              18.9: '9',
              19.4: '9-1_2',
              19.8: '10',
              20.2: '10-1_2'	
            }
            return calculateRingSize(sizeСhart, 15.3, 20.2, value);            
            break;
          case 'ring-inch':
            var sizeСhart = {
              0.60: '4-1_2',
              0.62: '5',
              0.63: '5-1_2',
              0.65: '6',
              0.67: '6-1_2',
              0.68: '7',
              0.70: '7-1_2',
              0.72: '8',
              0.73: '8-1_2',
              0.74: '9',
              0.76: '9-1_2',
              0.78: '10',
              0.80: '10-1_2'	
            }
            return calculateRingSize(sizeСhart, 0.60, 0.80, value);               
            break;
        }

      }
  })      


