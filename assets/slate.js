/*================ Slate ================*/

window.slate = window.slate || {};

/**
 * Utility helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions for dealing with arrays and objects
 *
 * @namespace utils
 */

slate.utils = {

    /**
     * Return an object from an array of objects that matches the provided key and value
     *
     * @param {array} array - Array of objects
     * @param {string} key - Key to match the value against
     * @param {string} value - Value to get match of
     */
    findInstance: function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
    },

    /**
     * Remove an object from an array of objects by matching the provided key and value
     *
     * @param {array} array - Array of objects
     * @param {string} key - Key to match the value against
     * @param {string} value - Value to get match of
     */
    removeInstance: function(array, key, value) {
        var i = array.length;
        while(i--) {
            if (array[i][key] === value) {
                array.splice(i, 1);
                break;
            }
        }

        return array;
    },

    /**
     * _.compact from lodash
     * Remove empty/false items from array
     * Source: https://github.com/lodash/lodash/blob/master/compact.js
     *
     * @param {array} array
     */
    compact: function(array) {
        var index = -1;
        var length = array == null ? 0 : array.length;
        var resIndex = 0;
        var result = [];

        while (++index < length) {
            var value = array[index];
            if (value) {
                result[resIndex++] = value;
            }
        }
        return result;
    },

    /**
     * _.defaultTo from lodash
     * Checks `value` to determine whether a default value should be returned in
     * its place. The `defaultValue` is returned if `value` is `NaN`, `null`,
     * or `undefined`.
     * Source: https://github.com/lodash/lodash/blob/master/defaultTo.js
     *
     * @param {*} value - Value to check
     * @param {*} defaultValue - Default value
     * @returns {*} - Returns the resolved value
     */
    defaultTo: function(value, defaultValue) {
        return (value == null || value !== value) ? defaultValue : value
    }
};

slate.Sections = function Sections() {
    this.constructors = {};
    this.instances = [];

    $(document)
        .on('shopify:section:load', this._onSectionLoad.bind(this))
        .on('shopify:section:unload', this._onSectionUnload.bind(this))
        .on('shopify:section:select', this._onSelect.bind(this))
        .on('shopify:section:deselect', this._onDeselect.bind(this))
        .on('shopify:section:reorder', this._onReorder.bind(this))
        .on('shopify:block:select', this._onBlockSelect.bind(this))
        .on('shopify:block:deselect', this._onBlockDeselect.bind(this));
};

slate.Sections.prototype = $.extend({}, slate.Sections.prototype, {
    _createInstance: function(container, constructor) {
        var $container = $(container);
        var id = $container.attr('data-section-id');
        var type = $container.attr('data-section-type');

        constructor = constructor || this.constructors[type];

        if (typeof constructor === 'undefined') {
            return;
        }

        var instance = $.extend(new constructor(container), {
            id: id,
            type: type,
            container: container
        });

        this.instances.push(instance);
    },

    _onSectionLoad: function(evt) {
        var container = $('[data-section-id]', evt.target)[0];
        if (container) {
            this._createInstance(container);
        }
    },

    _onSectionUnload: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (!instance) {
            return;
        }

        if (typeof instance.onUnload === 'function') {
            instance.onUnload(evt);
        }

        this.instances = slate.utils.removeInstance(this.instances, 'id', evt.detail.sectionId);
    },

    _onSelect: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (instance && typeof instance.onSelect === 'function') {
            instance.onSelect(evt);
        }
    },

    _onDeselect: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (instance && typeof instance.onDeselect === 'function') {
            instance.onDeselect(evt);
        }
    },

    _onReorder: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (instance && typeof instance.onReorder === 'function') {
            instance.onReorder(evt);
        }
    },

    _onBlockSelect: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (instance && typeof instance.onBlockSelect === 'function') {
            instance.onBlockSelect(evt);
        }
    },

    _onBlockDeselect: function(evt) {
        var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

        if (instance && typeof instance.onBlockDeselect === 'function') {
            instance.onBlockDeselect(evt);
        }
    },

    register: function(type, constructor) {
        this.constructors[type] = constructor;

        $('[data-section-type=' + type + ']').each(function(index, container) {
            this._createInstance(container, constructor);
        }.bind(this));
    }
});
