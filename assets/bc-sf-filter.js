// Override Settings
var bcSfFilterSettings = {
    general: {
        limit: bcSfFilterConfig.custom.products_per_page,
        /* Optional */
        //loadProductFirst: true,
        defaultDisplay: bcSfFilterConfig.custom.layout,
        paginationType: 'default',
        paginationTypeAdvanced: false,
    }
};

// Declare Templates
var bcSfFilterTemplate = {
    'soldOutClass': 'sold-out',
    'saleClass': 'on-sale',
    'soldOutLabelHtml': '<div>' + bcSfFilterConfig.label.sold_out + '</div>',
    'saleLabelHtml': '<div>' + bcSfFilterConfig.label.sale + '</div>',
    'vendorHtml': '<div>{{itemVendorLabel}}</div>',

    // Grid Template
    'productGridItemHtml': "<div class='product-grid-item grid__item sm--{{itemPerRowMobile}} md--up--{{itemPerRowDesktop}} mt3 lg--up--mt4'>" +
        '{{itemProductHtml}}' +
        '</div>',
    'productlistItemHtml': '<div class="item item--large-gap item--desktop--half item--mobile--full {{itemFirst}}  {{itemLast}}  {{itemFadeIn}}">' +
        '<div class="item__inner {{itemMobileClass}} {{itemDesktopClass}}">' +
        '{{itemProductHtml}}' +
        '</div>' +
        '</div>',

    'productItemHtml': '<a href="{{itemUrl}}" class="t--list-and-grid relative">' +
        '<div class="relative" data-limoniapps-discountninja-product-handle="{{itemHandle}}" >' +
        '{{itemImages}}' +
        '<p class="color--{{itemSectionColor}}-text m0 mt2 {{itemBasicFontSize}}">{{itemTitle}}</p>' +
        '{{itemPrice}}' +
        '</div>'+
        '</a>',

    // Pagination Template
    'previousActiveHtml': '<a class="p1 pointer" href="{{itemUrl}}" title=""><span class="font-size--xxxxs inline-block align--top border-bottom--transparent hv--border-bottom--{{itemSectionColor}}-text"><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-chevron-left" viewBox="0 0 64 64"><path fill="#162317" fill-rule="evenodd" d="M23.324 32L44.66 53.337l-2.666 2.666L18 32.008l.008-.007-.008-.008L41.993 8l2.666 2.666-21.335 21.336z"/></svg></span></a>',
    'previousDisabledHtml': '',
    'nextActiveHtml': '<a class="p1 pointer" href="{{{{itemUrl}}}}" title=""><span class="font-size--xxxxs inline-block align--top border-bottom--transparent hv--border-bottom--{{itemSectionColor}}-text"><svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-chevron-right" viewBox="0 0 64 64"><path fill="#162317" fill-rule="evenodd" d="M39.336 32L18 10.663l2.666-2.666L44.66 31.992l-.008.007.008.008L20.667 56 18 53.335l21.335-21.336z"/></svg></span></a>',
    'nextDisabledHtml': '',
    'pageItemHtml': '<a class="p1 pointer hv--span--border-bottom--{{itemSectionColor}}-text" href="{{itemUrl}}"><span class="border-bottom--transparent">{{itemTitle}}</span></a>',
    'pageItemSelectedHtml': '<a class="p1"><span class="border-bottom--{{itemSectionColor}}-text">{{itemTitle}}</span></a>',
    'pageItemRemainHtml': '<a class="p1 pointer" href="{{itemUrl}}"><span class="border-bottom--transparent">{{itemTitle}}</span></a>',
    'paginateHtml': '{{previous}}{{pageItems}}{{next}}',

    // Sorting Template
    'sortingHtml': ' <label class="sm--hide '+ bcSfFilterConfig.custom.global__basic_font_size +' mr1" for="SortBy">' + bcSfFilterConfig.label.sorting + '</label> <select class="bg--transparent border--'+ bcSfFilterConfig.custom.section_color +'-text color--'+ bcSfFilterConfig.custom.section_color +'-text p1 inline-block '+ bcSfFilterConfig.custom.global__basic_font_size +' pr3">{{sortingItems}}</select>',
};

/************************** BUILD PRODUCT LIST **************************/

// Build Product Grid Item
BCSfFilter.prototype.buildProductGridItem = function(data, index) {
    // Get Template
    var itemHtml = bcSfFilterTemplate.productGridItemHtml;
    itemHtml = itemHtml.replace(/{{itemProductHtml}}/g, buildProductHtml(data));

    var itemPerRowMobile = bcSfFilterConfig.custom.products_per_row_mobile;
    itemHtml = itemHtml.replace(/{{itemPerRowMobile}}/g, itemPerRowMobile);

    var itemPerRowDesktop = bcSfFilterConfig.custom.products_per_row_desktop;
    itemHtml = itemHtml.replace(/{{itemPerRowDesktop}}/g, itemPerRowDesktop);

    return itemHtml;
};
// Build Product List Item
BCSfFilter.prototype.buildProductListItem = function(data, index) {
    var itemHtml = bcSfFilterTemplate.productlistItemHtml;
    itemHtml = itemHtml.replace(/{{itemProductHtml}}/g, buildProductHtml(data));

    var itemFirst = (index == 1) ? 'item--first' : '';
    itemHtml = itemHtml.replace(/{{itemFirst}}/g, itemFirst);

    var itemLast = (index == bcSfFilterConfig.custom.products_per_page) ? 'item--last' : '';
    itemHtml = itemHtml.replace(/{{itemLast}}/g, itemFirst);

    var itemFadeIn = (bcSfFilterConfig.custom.layout) ? 'fade-in' : '';
    itemHtml = itemHtml.replace(/{{itemFadeIn}}/g, itemFadeIn);

    var itemMobileClass = (index % 2 === 0) ? 'md--dn--pl4' : 'md--dn--pr4';
    itemHtml = itemHtml.replace(/{{itemMobileClass}}/g, itemMobileClass);

    var itemDesktopClass = (bcSfFilterConfig.custom.col_count_desktop == 1) ? '' : 'lg--up--five-sixths lg--up--mx--auto';
    itemHtml = itemHtml.replace(/{{itemDesktopClass}}/g, itemDesktopClass);

    return itemHtml;
};
function buildProductHtml(data) {


    /*** Prepare data ***/
    var images = data.images_info;
    // data.price_min *= 100, data.price_max *= 100, data.compare_at_price_min *= 100, data.compare_at_price_max *= 100; // Displaying price base on the policy of Shopify, have to multiple by 100
    var soldOut = !data.available; // Check a product is out of stock
    var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
    var priceVaries = data.price_min != data.price_max; // Check a product has many prices

    // console.log('tpum em datan ' + JSON.stringify( bcSfFilterConfig.label, null, 2) );
    // Get First Variant (selected_or_first_available_variant)
    var firstVariant = data['variants'][0];
    if (getParam('variant') !== null && getParam('variant') != '') {
        var paramVariant = data.variants.filter(function(e) {
            return e.id == getParam('variant');
        });
        if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0];
    } else {
        for (var i = 0; i < data['variants'].length; i++) {
            if (data['variants'][i].available) {
                firstVariant = data['variants'][i];
                break;
            }
        }
    }
    /*** End Prepare data ***/
    var itemHtml = bcSfFilterTemplate.productItemHtml;
    // Add Thumbnail
    var itemImagesHtml = '';
    //images.length > 0 ? bcsffilter.optimizeImage(images[0]['src']) : bcSfFilterConfig.general.no_image_url;
    if (images.length > 0) {
        var aspect_ratio = images[0]['width'] / images[0]['height'];
        var force_crop = '';
        if (aspect_ratio < 0.5) {
            aspect_ratio = 1;
            force_crop = 'force-crop'
        }
        itemImagesHtml += '<div id="ProductImageWrapper-' + images[0].id + '" class="resp-img-wrapper ' + force_crop + ' js" >';
        itemImagesHtml += '<div style="padding-top:';
        itemImagesHtml += 1 / aspect_ratio * 100;
        itemImagesHtml += '%;">';
        itemImagesHtml += '<img id="ProductImage-' + images[0]['id'] + '" ' +
            'class="resp-img lazyload" ' +
            'width="' + images[0]['width'] + '"' +
            'height="' + images[0]['height'] + '"' +
            'src="' + bcsffilter.getFeaturedImage(images, '600x') + '" ' +
            'data-src="' + bcsffilter.getFeaturedImage(images, '{width}x') + '" ' +
            'data-widths="[180, 360, 470, 600, 750, 940, 1080, 1296, 1512, 1728, 2048]" ' +
            'data-aspectratio="' + aspect_ratio + '" ' +
            'data-ratio="' + aspect_ratio + '"' +
            'data-sizes="auto" ' +
            'alt="{{itemTitle}}">';
        itemImagesHtml += '<img id="ProductImage-' + images[0]['id'] + '" ' +
            'class="resp-img-placeholder"' +
            'src="' + bcsffilter.getFeaturedImage(images, '1x1') + '" ' +
            'width="' + images[0]['width'] + '"' +
            'height="' + images[0]['height'] + '"' +
            'alt="{{itemTitle}}">';
        itemImagesHtml += '</div>';
        if(images.length > 1 && bcSfFilterConfig.custom.hoverImages){
            itemImagesHtml += ' <div class="product-item-hover absolute top--0 right--0 bottom--0 left--0 z1 lazyload bg-cover"'+
                'data-bgset="'+bcsffilter.optimizeImage(images[1].src )+'"'+
                'data-sizes="auto"'+
                'data-parent-fit="cover"'+
                'style="background-image: url('+bcsffilter.optimizeImage(images[1].src)+');">'+
                '</div>';
        }
        itemImagesHtml += '</div>'
        itemImagesHtml += '<noscript><img src="' + bcsffilter.getFeaturedImage(images) + '" alt="" class="product__img"></noscript>';

    }
    itemHtml = itemHtml.replace(/{{itemImages}}/g, itemImagesHtml);
    // Add Price
    var itemPriceHtml = '';
    if (data.price_min > 0) {
        itemPriceHtml += '<p class=" ';
        itemPriceHtml += (onSale) ? 'color--{{itemSectionColor}}-accent' : 'color--{{itemSectionColor}}-meta';
        itemPriceHtml += 'm0  {{itemBasicFontSize}}">';
        if(soldOut){
            itemPriceHtml += '<span class="sold-out color--{{itemSectionColor}}-meta ';
            itemPriceHtml += (onSale) ? 'mr1' : '';
            itemPriceHtml += '">' + bcSfFilterConfig.label.sold_out + '</span>';
        } else {
            itemPriceHtml += '<span ';
            itemPriceHtml += (onSale) ? 'class="mr1" >' : ' class="priceToCheck limoniapps-discountninja-productprice limoniapps-discountninja-align-center">';
            if(onSale){
                if (priceVaries) {
                    itemPriceHtml += (bcSfFilterConfig.label.from).replace(/{{ price }}/g, bcsffilter.formatMoney(data.price_min));
                } else {
                    itemPriceHtml += bcsffilter.formatMoney(data.price_min);
                }
            } else {
                if (priceVaries) {
                    console.log('im nshac if i mej a ' + data.price_min +' '+ data.price_max);
                    console.log(bcSfFilterConfig.label.from);
                    itemPriceHtml += (bcSfFilterConfig.label.from).replace(/{{ price }}/g, bcsffilter.formatMoney(data.price_min));
                } else {
                    itemPriceHtml += bcsffilter.formatMoney(data.price_min);
                }
            }
            itemPriceHtml += '</span>';
        }
        if (onSale && !soldOut) {
            itemPriceHtml += '<s class="color--{{itemSectionColor}}-meta">' + bcsffilter.formatMoney(data.compare_at_price_min) + '</s> ';
        }

        itemPriceHtml += '</p>';
    }
    itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);


    // Add soldOut class
    var soldOutClass = soldOut ? bcSfFilterTemplate.soldOutClass : '';
    itemHtml = itemHtml.replace(/{{soldOutClass}}/g, soldOutClass);
    // Add onSale class
    var saleClass = onSale ? bcSfFilterTemplate.saleClass : '';
    itemHtml = itemHtml.replace(/{{saleClass}}/g, saleClass);
    // Add soldOut Label
    var itemSoldOutLabelHtml = soldOut ? bcSfFilterTemplate.soldOutLabelHtml : '';
    itemHtml = itemHtml.replace(/{{itemSoldOutLabel}}/g, itemSoldOutLabelHtml);
    // Add onSale Label
    var itemSaleLabelHtml = onSale ? bcSfFilterTemplate.saleLabelHtml : '';
    itemHtml = itemHtml.replace(/{{itemSaleLabel}}/g, itemSaleLabelHtml);
    // Add Vendor
    var itemVendorHtml = bcSfFilterConfig.custom.vendor_enable ? bcSfFilterTemplate.vendorHtml.replace(/{{itemVendorLabel}}/g, data.vendor) : '';
    itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

    var itemSectionColor = bcSfFilterConfig.custom.section_color ;
    itemHtml = itemHtml.replace(/{{itemSectionColor}}/g, itemSectionColor);

    var itemBasicFontSize = bcSfFilterConfig.custom.global__basic_font_size ;
    itemHtml = itemHtml.replace(/{{itemBasicFontSize}}/g, itemBasicFontSize);

    // Add main attribute (Always put at the end of this function)
    itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
    itemHtml = itemHtml.replace(/{{itemHandle}}/g, data.handle);
    itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
    itemHtml = itemHtml.replace(/{{itemUrl}}/g, bcsffilter.buildProductItemUrl(data));
    return itemHtml;
}
/************************** END BUILD PRODUCT LIST **************************/
// Build Pagination
BCSfFilter.prototype.buildPagination = function(totalProduct) {
    if (this.getSettingValue('general.paginationType') == 'default') {
        // Get page info
        var currentPage = parseInt(this.queryParams.page);
        var totalPage = Math.ceil(totalProduct / this.queryParams.limit);
        // If it has only one page, clear Pagination
        if (totalPage == 1) {
            jQ(this.selector.bottomPagination).html('');
            return false;
        }
        if (this.getSettingValue('general.paginationType') == 'default') {
            var paginationHtml = bcSfFilterTemplate.paginateHtml;
            // Build Previous
            var previousHtml = (currentPage > 1) ? bcSfFilterTemplate.previousActiveHtml : bcSfFilterTemplate.previousDisabledHtml;
            previousHtml = previousHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage - 1));
            paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);
            // Build Next
            var nextHtml = (currentPage < totalPage) ? bcSfFilterTemplate.nextActiveHtml : bcSfFilterTemplate.nextDisabledHtml;
            nextHtml = nextHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage + 1));
            paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);
            // Create page items array
            var beforeCurrentPageArr = [];
            for (var iBefore = currentPage - 1; iBefore > currentPage - 3 && iBefore > 0; iBefore--) {
                beforeCurrentPageArr.unshift(iBefore);
            }
            if (currentPage - 4 > 0) {
                beforeCurrentPageArr.unshift('...');
            }
            if (currentPage - 4 >= 0) {
                beforeCurrentPageArr.unshift(1);
            }
            beforeCurrentPageArr.push(currentPage);
            var afterCurrentPageArr = [];
            for (var iAfter = currentPage + 1; iAfter < currentPage + 3 && iAfter <= totalPage; iAfter++) {
                afterCurrentPageArr.push(iAfter);
            }
            if (currentPage + 3 < totalPage) {
                afterCurrentPageArr.push('...');
            }
            if (currentPage + 3 <= totalPage) {
                afterCurrentPageArr.push(totalPage);
            }
            // Build page items
            var pageItemsHtml = '';
            var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
            for (var iPage = 0; iPage < pageArr.length; iPage++) {
                if (pageArr[iPage] == '...') {
                    pageItemsHtml += bcSfFilterTemplate.pageItemRemainHtml;
                } else {
                    pageItemsHtml += (pageArr[iPage] == currentPage) ? bcSfFilterTemplate.pageItemSelectedHtml : bcSfFilterTemplate.pageItemHtml;
                }
                pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
                pageItemsHtml = pageItemsHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, pageArr[iPage]));
            }
            paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

            var itemSectionColor = bcSfFilterConfig.custom.section_color ;
            paginationHtml = paginationHtml.replace(/{{itemSectionColor}}/g, itemSectionColor);

            jQ(this.selector.bottomPagination).html(paginationHtml);
        }
    }
};
/************************** BUILD TOOLBAR **************************/
// Build Sorting
BCSfFilter.prototype.buildFilterSorting = function() {
    if (bcSfFilterTemplate.hasOwnProperty('sortingHtml')) {
        jQ(this.selector.topSorting).html('');
        var sortingArr = this.getSortingList();
        if (sortingArr) {
            // Build content
            var sortingItemsHtml = '';
            for (var k in sortingArr) {
                sortingItemsHtml += '<option value="' + k + '">' + sortingArr[k] + '</option>';
            }
            var html = bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g, sortingItemsHtml);
            jQ(this.selector.topSorting).html(html);
            // Set current value
            jQ(this.selector.topSorting + ' select').val(this.queryParams.sort);
        }
    }
};
// Build Display type (List / Grid / Collage)
// BCSfFilter.prototype.buildFilterDisplayType = function() {
//     var itemHtml = '<a href="' + this.buildToolbarLink('display', 'list', 'grid') + '" title="Grid view" class="change-view bc-sf-filter-display-grid" data-view="grid"><span class="icon-fallback-text"><i class="fa fa-th" aria-hidden="true"></i><span class="fallback-text">Grid view</span></span></a>';
//     itemHtml += '<a href="' + this.buildToolbarLink('display', 'grid', 'list') + '" title="List view" class="change-view bc-sf-filter-display-list" data-view="list"><span class="icon-fallback-text"><i class="fa fa-list" aria-hidden="true"></i><span class="fallback-text">List view</span></span></a>';
//     jQ(this.selector.topDisplayType).html(itemHtml);
//     // Active current display type
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').removeClass('active');
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').removeClass('active');
//     if (this.queryParams.display == 'list') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').addClass('active');
//     } else if (this.queryParams.display == 'grid') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').addClass('active');
//     }
// };
/************************** END BUILD TOOLBAR **************************/
// Build Default layout
function buildDefaultLink(a, b) {
    var c = window.location.href.split("?")[0];
    return c += "?" + a + "=" + b
}
BCSfFilter.prototype.buildDefaultElements = function(a) {
    if (bcSfFilterConfig.general.hasOwnProperty("collection_count") && jQ("#bc-sf-filter-bottom-pagination").length > 0) {
        var b = bcSfFilterConfig.general.collection_count,
            c = parseInt(this.queryParams.page),
            d = Math.ceil(b / this.queryParams.limit);
        if (1 == d) return jQ(this.selector.pagination).html(""), !1;
        if ("default" == this.getSettingValue("general.paginationType")) {
            var e = bcSfFilterTemplate.paginateHtml,
                f = "";
            f = c > 1 ? bcSfFilterTemplate.hasOwnProperty("previousActiveHtml") ? bcSfFilterTemplate.previousActiveHtml : bcSfFilterTemplate.previousHtml : bcSfFilterTemplate.hasOwnProperty("previousDisabledHtml") ? bcSfFilterTemplate.previousDisabledHtml : "", f = f.replace(/{{itemUrl}}/g, buildDefaultLink("page", c - 1)), e = e.replace(/{{previous}}/g, f);
            var g = "";
            g = c < d ? bcSfFilterTemplate.hasOwnProperty("nextActiveHtml") ? bcSfFilterTemplate.nextActiveHtml : bcSfFilterTemplate.nextHtml : bcSfFilterTemplate.hasOwnProperty("nextDisabledHtml") ? bcSfFilterTemplate.nextDisabledHtml : "", g = g.replace(/{{itemUrl}}/g, buildDefaultLink("page", c + 1)), e = e.replace(/{{next}}/g, g);
            for (var h = [], i = c - 1; i > c - 3 && i > 0; i--) h.unshift(i);
            c - 4 > 0 && h.unshift("..."), c - 4 >= 0 && h.unshift(1), h.push(c);
            for (var j = [], k = c + 1; k < c + 3 && k <= d; k++) j.push(k);
            c + 3 < d && j.push("..."), c + 3 <= d && j.push(d);
            for (var l = "", m = h.concat(j), n = 0; n < m.length; n++) "..." == m[n] ? l += bcSfFilterTemplate.pageItemRemainHtml : l += m[n] == c ? bcSfFilterTemplate.pageItemSelectedHtml : bcSfFilterTemplate.pageItemHtml, l = l.replace(/{{itemTitle}}/g, m[n]), l = l.replace(/{{itemUrl}}/g, buildDefaultLink("page", m[n]));
            e = e.replace(/{{pageItems}}/g, l), jQ(this.selector.pagination).html(e)
        }
    }
    if (bcSfFilterTemplate.hasOwnProperty("sortingHtml") && jQ(this.selector.topSorting).length > 0) {
        jQ(this.selector.topSorting).html("");
        var o = this.getSortingList();
        if (o) {
            var p = "";
            for (var q in o) p += '<option value="' + q + '">' + o[q] + "</option>";
            var r = bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g, p);
            jQ(this.selector.topSorting).html(r);
            var s = void 0 !== this.queryParams.sort_by ? this.queryParams.sort_by : this.defaultSorting;
            jQ(this.selector.topSorting + " select").val(s), jQ(this.selector.topSorting + " select").change(function(a) {
                window.location.href = buildDefaultLink("sort_by", jQ(this).val())
            })
        }
    }
};
// Customize data to suit the data of Shopify API
BCSfFilter.prototype.prepareProductData = function(data) {
    for (var k = 0; k < data.length; k++) {
        data[k]['images'] = data[k]['images_info'];
        if (data[k]['images'].length > 0) {
            data[k]['featured_image'] = data[k]['images'][0]
        } else {
            data[k]['featured_image'] = {
                src: bcSfFilterConfig.general.no_image_url,
                width: '',
                height: '',
                aspect_ratio: 0
            }
        }
        data[k]['url'] = '/products/' + data[k].handle;
        var optionsArr = [];
        for (var i = 0; i < data[k]['options_with_values'].length; i++) {
            optionsArr.push(data[k]['options_with_values'][i]['name'])
        }
        data[k]['options'] = optionsArr;
        data[k]['price_min'] *= 100, data[k]['price_max'] *= 100, data[k]['compare_at_price_min'] *= 100, data[k]['compare_at_price_max'] *= 100;
        data[k]['price'] = data[k]['price_min'];
        data[k]['compare_at_price'] = data[k]['compare_at_price_min'];
        data[k]['price_varies'] = data[k]['price_min'] != data[k]['price_max'];
        var firstVariant = data[k]['variants'][0];
        if (getParam('variant') !== null && getParam('variant') != '') {
            var paramVariant = data.variants.filter(function(e) {
                return e.id == getParam('variant')
            });
            if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0]
        } else {
            for (var i = 0; i < data[k]['variants'].length; i++) {
                if (data[k]['variants'][i].available) {
                    firstVariant = data[k]['variants'][i];
                    break
                }
            }
        }
        data[k]['selected_or_first_available_variant'] = firstVariant;
        for (var i = 0; i < data[k]['variants'].length; i++) {
            var variantOptionArr = [];
            var count = 1;
            var variant = data[k]['variants'][i];
            var variantOptions = variant['merged_options'];
            if (Array.isArray(variantOptions)) {
                for (var j = 0; j < variantOptions.length; j++) {
                    var temp = variantOptions[j].split(':');
                    data[k]['variants'][i]['option' + (parseInt(j) + 1)] = temp[1];
                    data[k]['variants'][i]['option_' + temp[0]] = temp[1];
                    variantOptionArr.push(temp[1])
                }
                data[k]['variants'][i]['options'] = variantOptionArr
            }
            data[k]['variants'][i]['compare_at_price'] = parseFloat(data[k]['variants'][i]['compare_at_price']) * 100;
            data[k]['variants'][i]['price'] = parseFloat(data[k]['variants'][i]['price']) * 100
        }
        data[k]['description'] = data[k]['content'] = data[k]['body_html']
    }
    return data
};
// Add additional feature for product list, used commonly in customizing product list
BCSfFilter.prototype.buildExtrasProductList = function(data, eventType) {
    $('.collection').find('#bc-sf-filter-products').find(".item").addClass("ready");
    if ($('.collection').data("fade-in") === true) {
        theme.fadeIn('.collection');
    }
};
// Build additional elements
BCSfFilter.prototype.buildAdditionalElements = function(data, eventType) {
    $(".product-grid-item").on({
        mouseenter: function () {
            $(this).find('.product-item-hover ').addClass('show');
        },
        mouseleave: function () {
            $(this).find('.product-item-hover ').removeClass('show');
        }
    });
  
    $(window).unbind('scroll');
    jQ('.loadMoreNewPaging').remove();
  
  	addFiltersEventListeners();
};

BCSfFilter.prototype.buildProductListData = function(data, eventType) {
    var $ = jQuery;

    if (this.getSettingValue('general.paginationType') == 'default' || (this.getSettingValue('general.paginationType') != 'default' && eventType != 'page')) {
        jQ(this.getSelector('products')).html('');
        jQ(window).unbind('scroll');
    }

    var itemsHtml = '';
    var totalProduct = data.length;

    if (this.queryParams.display == 'list') {
        itemsHtml = '<div class="item-sizer"></div><div class="column-sizer"></div>' + itemsHtml;
    }
    for (var i = 0; i < totalProduct; i++) {
        switch(this.queryParams.display) {
            case 'list': var itemHtml = this.buildProductListItem(data[i], i + 1, totalProduct); break;
            case 'collage': var itemHtml = this.buildProductCollageItem(data[i], i + 1, totalProduct); break;
            default: var itemHtml = this.buildProductGridItem(data[i], i + 1, totalProduct); break;
        }
        itemsHtml += this.buildProductItemAdvanced(data[i], itemHtml);
    }

    if(this.queryParams.display == 'list'){
        var $newMasonryContent = $(itemsHtml);
        $(this.getSelector('products')).append($newMasonryContent).masonry('prepended', $newMasonryContent );
    } else {
        $(this.getSelector('products')).append(itemsHtml);
    }

};


// Fix image url issue of swatch option
function getFilePath(fileName, ext, version) {
    var self = bcsffilter;
    var ext = typeof ext !== 'undefined' ? ext : 'png';
    var version = typeof version !== 'undefined' ? version : '1';
    var prIndex = self.fileUrl.lastIndexOf('?');
    if (prIndex > 0) {
        var filePath = self.fileUrl.substring(0, prIndex);
    } else {
        var filePath = self.fileUrl;
    }
    filePath += fileName + '.' + ext + '?v=' + version;
    return filePath;
}

// Build Infinite Loading event
BCSfFilter.prototype.buildInfiniteLoadingEvent = function(data) {
  var self = this;
  // Get total products
  var totalProduct = parseInt(data.total_product);
  /* Get limit per page */
  var limitPerPage = parseInt(self.getSettingValue('general.limit'));
  var currentPage = parseInt(self.queryParams.page);
  /* Check if total products is greater than limit per page * curent page, do the infinite loading function, else do nothing */
  if (totalProduct > (limitPerPage * currentPage)) {

    // Using scrolling to prevent callling onscroll twice
    var scrolling = 0;

    // onscroll event
    // When windown reaches the bottom of product list
    var scrollToBottom = false;

    // Pagination selector
    var paginationSelector = jQ(self.getSelector('bottomPagination'));
    
    // Check if pagination exists, do the infinite function
    if (paginationSelector.length > 0) {
      // When scroll down, do the infinite function
      jQ(window).scroll(function(e) {
        e.preventDefault();
        e.stopPropagation();

        // Ignore load more if the product list is loading
        if (jQ(self.selector.products).hasClass('bc-sf-product-loading')) return false;

        // get height of scroll bar
        var scrollbarHeight = jQ(window).height() * (jQ(window).height() / jQ(document).outerHeight());

        // get position of the pagination bottom
        var maxPos = parseInt(paginationSelector.offset().top);
        var maxScroll = parseInt(jQ(window).scrollTop()) + scrollbarHeight + self.getSettingValue('general.positionShowInfiniteLoading');
        //console.log(scrollbarHeight, maxPos, maxScroll)
        //console.log((jQ(window).scrollTop() + jQ(window).height() + scrollbarHeight), jQ(document).outerHeight() - 100)
        if ((jQ(window).scrollTop() + jQ(window).height() + scrollbarHeight) >= jQ(document).outerHeight() - 100) {
          scrollToBottom = true;
        }
        console.log(scrollToBottom)
        // Begin loading more products
        if (scrolling == 0 && data.products.length > 0) {
          if (/*maxScroll >= maxPos ||*/ /*(maxScroll < maxPos &&*/ scrollToBottom/*)*/) {
            // Show Loading
            self.showLoadMoreLoading();

            // Prevent scrolling twice
            scrolling = 1;

            // Next page
            currentPage++;

            // Send request if current page is smaller or equal to total number of pages
            var totalPage = Math.ceil(data.total_product / self.getSettingValue('general.limit'));
            // console.log(currentPage, totalPage);
            if (currentPage <= totalPage) {
              self.internalClick = true;
              self.queryParams.limit = self.getSettingValue('general.limit');
              self.queryParams.page = currentPage;
              // Append "page" param to url
              // Used to Scroll to the previous position after returning from Product page
              if (self.getSettingValue('general.paginationTypeAdvanced')) {
                var newUrl = self.buildToolbarLink('page', currentPage - 1, currentPage);
                self.onChangeData(newUrl, 'page');
              } else {
                self.getFilterData('page');
              }
            }
          }
        }
      });
    }
  }
};

BCSfFilter.prototype.getFilterData = function(eventType, errorCount) {
  var self = bcsffilter;
  jQ(self.selector.products).addClass('bc-sf-product-loading');
  function BCSend(eventType, errorCount) {
    
    var self = bcsffilter;
    var errorCount = typeof errorCount !== 'undefined' ? errorCount : 0;
    // Show Loading icon
    self.showLoading();
    //build Placeholder for product list
    self.buildPlaceholderProductList(eventType);
    // Action before requesting API
    self.beforeGetFilterData(eventType);
    // Prepare request params
    self.prepareRequestParams(eventType);
    self.queryParams['callback'] = 'BCSfFilterCallback';
    self.queryParams['event_type'] = eventType;
    // Prepare url
    var url = self.isSearchPage() ? self.getApiUrl('search') : self.getApiUrl('filter');
    // Create request
    var script = document.createElement("script");
    script.type = 'text/javascript';
    var timestamp = new Date().getTime();
    script.src = url + '?t=' + timestamp + '&' + jQ.param(self.queryParams);
    script.id = 'bc-sf-filter-script';
    script.async = true;
    var resendAPITimer,
      resendAPIDuration = 2000;
    script.addEventListener('error', function(e) {
      if (typeof document.getElementById(script.id).remove == 'function') {
        // If support is found
        document.getElementById(script.id).remove();
      } else {
        // If not
        document.getElementById(script.id).outerHTML = '';
      }
      // Resend API 2 times when got the error
      if (errorCount < 2) {
        errorCount++;
        if (resendAPITimer) {
          clearTimeout(resendAPITimer);
        }
        resendAPITimer = setTimeout(self.getFilterData('resend', errorCount), resendAPIDuration);        
      } else {
        self.buildDefaultElements(eventType);
      }
    });
    // Append the script element to the HEAD section.
    document.getElementsByTagName('head')[0].appendChild(script);
    script.onload = function() {
      if (typeof document.getElementById(script.id).remove == 'function') {
        // If support is found
        document.getElementById(script.id).remove();
      } else {
        // If not
        document.getElementById(script.id).outerHTML = '';
      }
    };
  }

  this.requestFilter(BCSend, eventType, errorCount);
  setTimeout(function(){ jQ(self.selector.products).removeClass('bc-sf-product-loading'); }, 3000);
  
};

/* Begin patch boost-010 run 2 */
BCSfFilter.prototype.initFilter=function(){return this.isBadUrl()?void(window.location.href=window.location.pathname):(this.updateApiParams(!1),void this.getFilterData("init"))},BCSfFilter.prototype.isBadUrl=function(){try{var t=decodeURIComponent(window.location.search).split("&"),e=!1;if(t.length>0)for(var i=0;i<t.length;i++){var n=t[i],a=(n.match(/</g)||[]).length,r=(n.match(/>/g)||[]).length,o=(n.match(/alert\(/g)||[]).length,h=(n.match(/execCommand/g)||[]).length;if(a>0&&r>0||a>1||r>1||o||h){e=!0;break}}return e}catch(l){return!0}};
/* End patch boost-010 run 2 */

BCSfFilter.prototype.onChangeData = function(url, eventType, fOValue, fOId) {
    this.updateApiParams(url);
    this.getFilterData(eventType);
    this.changeAddressBar(url, eventType, fOValue);
  	jQ('.vwo_filtersApplied').remove();
};


BCSfFilter.prototype.buildFilterOption = function(content, data, fTreeType) {
  console.log('build filter option', data.label);
  if (data.label == 'Size') console.log(content);
    var blockTitle = data.label;
    var blockId = this.class.filterOption + '-' + this.slugify(blockTitle);
    // Build wrapper of a filter option
    var html = fTreeType == 'vertical' ? this.getTemplate('filterOptionWrapper') : this.getTemplate('filterOptionHorizontalWrapper');
    html = html.replace(/{{blockTitle}}/g, blockTitle);
    html = html.replace(/{{blockTitleId}}/g, this.class.filterBlockTitle + '-' + this.slugify(blockTitle));
    html = html.replace(/{{blockTypeClass}}/g, this.class.filterOption + '-' + data.displayType);
    html = html.replace(/{{blockId}}/g, blockId);
    html = html.replace(/{{blockContent}}/g, content);
    html = html.replace(/{{blockContentId}}/g, this.class.filterBlockContent + '-' + this.slugify(blockTitle));
    html = html.replace(/{{btnApply}}/g, (data.filterType == 'collection' || data.filterType == 'review_ratings') ? '' : '<button class="' + this.class.selectFilterOptionButton + '" data-id="' + blockId + '">' + this.getSettingValue('label.apply') + '</button>');
    // Tooltip
    html = html.replace(/{{tooltip}}/g, this.buildFilterOptionTooltip(data));
    // Add Clear button to a filter option block when URL has param like type of the filter option
    // Except type of filter option is collection
    var clearButtonHtml = '';
    var clearButtonLabel = fTreeType == 'vertical' ? this.getSettingValue('label.clear') : this.getSettingValue('label.clearAll');
    if (this.queryParams.hasOwnProperty(data.filterOptionId) && this.queryParams[data.filterOptionId]) {
        clearButtonHtml = '<a href="javascript:;" class="' + this.class.clearButton + '" onClick="clearFilterOption(event, this, \'' + data.filterOptionId + '\')">' + clearButtonLabel + '</a>';
    }
    html = html.replace(/{{clearButton}}/g, clearButtonHtml);
    // Add common class
    html = this.buildFilterTreeClass(html);
    if (html !== '') {
        var htmlElement = jQ(html);
        // Add additional attribute
        htmlElement.attr({
            'data-id': data.filterOptionId,
            'data-type': data.filterType,
            'data-display-type': data.displayType,
            'data-select-type': data.selectType,
            'data-title': blockTitle,
            'data-prefix': data.prefix
        });
        // Collapse each Filter options on Mobile
        // If collapseOnMobileByDefault is turned on true, all filter options will be collapsed, even though they are set on the filter option setting.
        var isCollapseMobile = data.hasOwnProperty('isCollapseMobile') ? data.isCollapseMobile : false;
        if (this.getSettingValue('general.collapseOnMobileByDefault')) isCollapseMobile = true;
        htmlElement.attr('data-collapse-mobile', isCollapseMobile);
        // Add Show More
        var showMoreType = null;
        if (['list', 'box', 'swatch'].indexOf(data.displayType) > -1) showMoreType = 'scrollbar';
        if (data.hasOwnProperty('showMoreType') && data.showMoreType) showMoreType = data.showMoreType;
        if (this.getSettingValue('general.showMoreType') != '' && ['list'].indexOf(data.displayType) > -1) showMoreType = this.getSettingValue('general.showMoreType');
        if (showMoreType) htmlElement.attr({ 'data-show-more-type': showMoreType });

        // Do filter as soon as clicking on the filter option item
        // And remove the Apply button
        if (this.getSettingValue('general.requestInstantly')) htmlElement.find('.' + this.class.selectFilterOptionButton).hide();

        // Add attribute for AND condition
        if (data.hasOwnProperty('useAndCondition') && data.useAndCondition) htmlElement.attr('data-and-condition', true);

        // Add attribute for Exact Rating
        if(data.hasOwnProperty('showExactRating') && data.showExactRating) htmlElement.attr('data-exact-rating', true);

        // Add Filter option to Filter tree
        this.addFilterTreeItem(jQ(htmlElement)[0].outerHTML, 'after', fTreeType);
if (data.label == 'Size') console.log(jQ(htmlElement)[0].outerHTML);
        // Hide Filter options
        if (!this.checkShowFilterOption(data)) jQ('.' + blockId).addClass(this.class.filterOptionHidden);
    }
};