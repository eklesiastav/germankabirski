var glob = require('glob');

module.exports = {
    mode: "development",
    entry: toObject(glob.sync('./assets/*.js')),
    output: {
        filename: 'app.js'
    },
};

function toObject(paths) {
    var ret = {};

    paths.forEach(function(path) {
        // you can define entry names mapped to [name] here
        ret[path.split('/').slice(-1)[0]] = path;
    });

    return ret;
}